<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::post('/login/admin', 'Auth\LoginController@adminLogin');

//Admin routes
Route::group(['middleware' => ['auth:admin'], 'prefix' => 'admin'], function(){

	Route::get('/', 'AdminsController@index')->name('admin.index');
	//Get users
	Route::get('user', 'AdminsController@getUsers')->name('admin.user.index');
	//Get Items
	Route::get('item', 'AdminsController@getItems')->name('admin.item.index');
	//Get checklists
	Route::get('checklist', 'AdminsController@getChecklists')->name('admin.checklist.index');

	Route::get('checklist/{id}/show', 'AdminsController@getChecklistById')->name('admin.checklist.show');

	Route::get('user/{id}/task', 'AdminsController@getUserTasks')->name('admin.user.task.index');
	

});

Route::group(['middleware' => ['roles:owner|manager']], function(){
	//Item resource controller
	Route::resource('item', 'ItemsController')->except(['show', 'destroy']);

	//Checklist resource controller
	Route::resource('checklist', 'ChecklistsController')->except([ 'destroy']);

	//Checklist resource controller
	Route::post('pre/checklist/{id}/use_again', 'ChecklistsController@useAgainCheclistById')->name('checklist.use_again');
	//Check task route
	Route::get('check/task', 'TasksController@checkTasks')->name('check.task.index');

});

Route::group(['middleware' => ['roles:owner|manager|cleaning|renter']], function(){
	//task route
	Route::get('task', 'TasksController@index')->name('task.index');
	Route::get('task/create/checklist/{checklist_id}', 'TasksController@create')->name('task.create');
	Route::post('task/store', 'TasksController@store')->name('task.store');
});