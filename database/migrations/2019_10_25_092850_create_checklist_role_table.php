<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecklistRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_role', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('checklist_id')->unsigned();  
            $table->integer('role_id')->unsigned();  
            
            $table->foreign('checklist_id')
              ->references('id')->on('checklists')
              ->onDelete('cascade');
            $table->foreign('role_id')
              ->references('id')->on('roles')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklist_role');
    }
}
