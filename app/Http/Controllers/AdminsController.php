<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Item;
use App\Models\Checklist;

class AdminsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }

    /**
     *  get all users
     *
     *  @method getUsers
     *
     *  @return [type] [description]
     */
    
    public function getUsers(){

        $users = User::with('roles')->with('tasks')->get();//users with their roles
        // dd($users);
        return view('admin.users.index', compact('users'));
    }

    /**
     *  Get user tasks
     *
     *  @method getUserTasks
     *
     *  @param  [type] $user_id [description]
     *
     *  @return [type] [description]
     */
    
    public function getUserTasks($user_id){

        $user = User::with('tasks')->find($user_id); //get user task by userid

        return view('admin.users.task_list', compact('user'));
    }

    /**
     *  Get all Items
     *
     *  @method getItems
     *
     *  @return [type] [description]
     */
    
    public function getItems(){

        $data = Item::get(); //get all items

        return view('admin.item_list', compact('data'));
    }

    /**
     *  get all checklist
     *
     *  @method getChecklists
     *
     *  @return [type] [description]
     */
    
    public function getChecklists(){

        $data = Checklist::get(); //get all checklist

        return view('admin.checklist_list', compact('data'));
    }

    /**
     *  Get teh checlist preview
     *
     *  @method getChecklistById
     *
     *  @param  [type] $id [description]
     *
     *  @return [type] [description]
     */
    
    public function getChecklistById($id){

        $items = Item::get();
        $checklist = Checklist::find($id);

        return view('admin.checklist_show', compact('items', 'checklist', 'roles'));
    }
}
