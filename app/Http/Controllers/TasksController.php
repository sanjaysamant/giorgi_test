<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Checklist;
use App\Models\Task;
use Auth;
use DB;

class TasksController extends Controller
{	
	protected $taskModel;

    public function __construct(){

    	$this->taskModel = new Task; //object initiating of Task model
    }

    /**
     *  [List checklist items with completed and incompleted task]
     *
     *  @method index
     *
     *  @return [type] [description]
     */
    
    public function index(){

        // $for_renter = false;//assign a boolean false
        // if(Auth::user()->hasRole('renter'))//check if role is renter
        //     $for_renter = true;

    	// $task = null;
    	//get the checklist with items of checklist within a time duration
        $checklists = Checklist::with('items')
            ->whereDate('start_date', '<=', date('Y-m-d'))
            ->whereDate('end_date', '>=', date('Y-m-d'))
            ->whereHas('roles', function($que){

                return $que->whereHas('users', function($qr){

                    return $qr->where('users.id', Auth::id());
                });
            })
            ->with(['task' => function($query){
                if(Auth::user()->hasRole('owner')){

                    $query->whereHas('user', function($qry){

                        return $qry->where('users.id', Auth::id());
                    })->with('items');
                }
                else{

                    $query->whereHas('user', function($qry){

                        return $qry->whereHas('roles', function($q){

                            return $q->where('name', '!=', 'owner');
                        });
                    })->with('items');
                }
            }])->get();
        // dd($checklists);
    	if(!$checklists->count()){

    		return redirect()->back()->with('errors', 'No data');
	    }
    	//get the task if task is checked with items
    	// $task = $this->taskModel->item_task($checklist->id, Auth::id())->where('checklist_id', $checklist->id)->first();

    	// dd($task);
    	return view('tasks.index', compact('checklists'));
    }

    /**
     *  [create tasks with adding items of checklist]
     *
     *  @method create
     *
     *  @param  [type] $checklist_id [description]
     *
     *  @return [type] [description]
     */
    
    public function create($checklist_id){

        // $for_renter = false;//assign a boolean false
        // if(Auth::user()->hasRole('renter'))//check if role is renter
        //     $for_renter = true;

    	$task = null;
    	//get the checklist with items of checklist within a time duration
    	$checklist = Checklist::with('items')->find($checklist_id);
    	if(!$checklist){

    		return redirect()->back()->with('errors', 'No data');
	    }
    	//get the task if task is checked with items
    	$task = $this->taskModel->item_task($checklist->id, Auth::id())->where('checklist_id', $checklist->id)->first();
        // dd($task);

    	return view('tasks.create', compact('checklist', 'task'));
    }

    /**
     *  store the task and items
     *
     *  @method store
     *
     *  @param  Request $request [description]
     *
     *  @return [type] [description]
     */
    
    public function store(Request $request){

    	//create the task
    	$task = Task::where('user_id', Auth::id())->updateOrCreate(['user_id' => Auth::id(), 'checklist_id' => $request->checklist_id ]);
    	//Detaching the task items
		DB::table('item_task')
            ->where('task_id', $task->id)
                ->where('checklist_id', $request->checklist_id)
			        ->delete();
    	if(!empty($request->item_id)){

	    	foreach ($request->item_id as $item) {
	    		//Inserting the task items
    			DB::table('item_task')->where('task_id', $task->id)
                    ->where('checklist_id', $request->checklist_id)
    					->updateOrInsert(['task_id' => $task->id, 'checklist_id' => $request->checklist_id, 'item_id' => $item]);
	    	}
	    }

    	return redirect()->route('task.index');
    }

    /**
     *  Check tasks done by members
     *
     *  @method checkTasks
     *
     *  @return [type] [description]
     */
    
    public function checkTasks(){

        // $for_renter = false;//assign a boolean false
        // if(Auth::user()->hasRole('renter'))//check if role is renter
        //     $for_renter = true;

        // $task = null;
        //get the checklist with items of checklist within a time duration
        $sql = Task::whereNotNull('id');
        if(Auth::user()->hasRole('owner')){

            $sql->with('user');
        }
        else{

            $sql->whereHas('user', function($qry){

                return $qry->whereHas('roles', function($q){

                    return $q->where('name', '!=', 'owner');
                });
            });
        }

        $tasks = $sql->with('checklist')->get();


        // $checklists = Checklist::with('items')
        //     ->whereDate('start_date', '<=', date('Y-m-d'))
        //     ->whereDate('end_date', '>=', date('Y-m-d'))
        //     ->with(['task' => function($query){
        //         if(Auth::user()->hasRole('owner')){

        //             $query->whereHas('user', function($qry){

        //                 return $qry->where('users.id', Auth::id());
        //             })->with('items');
        //         }
        //         else{

        //             $query->whereHas('user', function($qry){

        //                 return $qry->whereHas('roles', function($q){

        //                     return $q->where('name', '!=', 'owner');
        //                 });
        //             })->with('items');
        //         }
        //     }])->get();
        // dd($checklists);
        if(!$tasks->count()){

            return redirect()->back()->with('errors', 'No data');
        }
        //get the task if task is checked with items
        // $query = $this->taskModel->where('checklist_id', $checklist->id);
        // if(Auth::user()->hasRole('owner')){

        //     $query->with('user');
        // }
        // else{

        //     $query->whereHas('user', function($qry){

        //         return $qry->whereHas('roles', function($q){

        //             return $q->where('name', '!=', 'owner');
        //         });
        //     });
        // }
        // $tasks = $query->with('checklist')->get();

        // dd($tasks);
        return view('tasks.check_tasks', compact('tasks'));
    }
}
