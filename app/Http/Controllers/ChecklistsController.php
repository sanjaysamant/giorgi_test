<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Checklist;
use App\Models\Item;
use App\Models\Role;
use Carbon\Carbon;
use Auth;

class ChecklistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $for_renter = false;//assign a boolean false
        // if(Auth::user()->hasRole('renter'))//check if role is renter
        //     $for_renter = true;
        $data = Checklist::get(); //get all checklist

        return view('checklists.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::get();//Get all items
        $query = Role::whereNotNull('id');
        if(!Auth::user()->hasRole('owner'))//Cjeck if the logged in user is owner
            $query->where('name', '!=', 'owner');
        // if(Auth::user()->hasRole('renter'))//Check if the current user is renter
        //     $query->where('name', '=', 'renter');
        // else
            // $query->where('name', '!=', 'renter');
        $roles = $query->get();//Get all roles

        return view('checklists.create', compact('items', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $for_renter = false;//assign a boolean false
        // if(Auth::user()->hasRole('renter'))//check if role is renter
        //     $for_renter = true;

        $checklist = ['name' => $request->name, 'start_date' => $request->start_date, 'end_date' => $request->end_date,];
        // dd($checklist);
        $data = Checklist::create($checklist);//store checklist

        $data->items()->attach($request->item_id);//store checklist item to pivot
        $data->roles()->attach($request->role_id);//assign checklist to role

        return redirect()->route('checklist.index')->with('success', 'Item created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items = Item::get();
        $checklist = Checklist::find($id);
        $query = Role::whereNotNull('id');
        if(!Auth::user()->hasRole('owner'))//Cjeck if the logged in user is owner
            $query->where('name', '!=', 'owner');
        // if(Auth::user()->hasRole('renter'))//Check if the current user is renter
        //     $query->where('name', '=', 'renter');
        // else
        //     $query->where('name', '!=', 'renter');

        $roles = $query->get();//Get all roles

        return view('checklists.edit', compact('items', 'checklist', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $for_renter = false;//assign a boolean false
        // if(Auth::user()->hasRole('renter'))//check if role is renter
        //     $for_renter = true;

        $data = Checklist::find($id);//find checklist by id
        $data->name = $request->name;
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;
        // $data->for_renter = $for_renter;
        $data->save();
        // dd($data->items);

        $data->items()->sync($request->item_id);//update checklist item to pivot
        $data->roles()->sync($request->role_id);//assign checklist to role

        return redirect()->route('checklist.index')->with('success', 'Item updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    // }
    
    /**
     *  [useAgainCheclistById description]
     *
     *  @method useAgainCheclistById
     *
     *  @param  Request $request [description]
     *  @param  [type] $id [description]
     *
     *  @return [type] [description]
     */
    
    public function useAgainCheclistById(Request $request, $id){

        $items = [];
        $roles = [];
        $data = Checklist::with('roles')->with('items')->find($id);// get the checklist
        $start_date = Carbon::createFromFormat('Y-m-d', $data->start_date);
        $end_date = Carbon::createFromFormat('Y-m-d', $data->end_date);
        $diff = $end_date->diffInDays($start_date);//calculate the days between startdate and enddate
        foreach ($data->items as $item) {

            $items[] = $item->id;//add checklist items in an array
        }
        foreach ($data->roles as $role) {

            $roles[] = $role->id;//add checklist role in an array
        }
        // dd($roles);
        //create checklist
        $checklist = Checklist::create(['name' => $data->name, 'start_date' => date('Y-m-d', strtotime($data['end_date']. ' + 1 days')), 'end_date' => date('Y-m-d', strtotime($data['end_date']. ' + ' . $diff . ' days')),]);
        $checklist->items()->attach($items);
        $checklist->roles()->attach($roles);//assign checklist to role

        return redirect()->back()->with('success', 'Checklist added');
    } 
    
}
