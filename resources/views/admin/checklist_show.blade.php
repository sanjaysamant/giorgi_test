@extends('admin.layouts.master')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Checklist</strong> show
            </div>

            @if ($errors->any())
                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                    <span class="badge badge-pill badge-danger"></span>
                    {{ implode('', $errors->all(':message')) }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif          
            <div class="card-body card-block">
                     <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="name" class=" form-control-label">Checklist Name*</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <p>{{$checklist->name}}</p>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="title" class=" form-control-label">Start Date*</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <p>{{$checklist->start_date}}</p>                            
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="title" class=" form-control-label">End Date*</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <p>{{$checklist->end_date}}</p>                            
                            
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="role_id" class=" form-control-label">For Role</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <p>{{implode(', ', $checklist->roles->pluck('name')->toArray())}}</p>
                            
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="select" class=" form-control-label">Items</label>
                        </div>
                        <div class="col-12 col-md-9">
                                <div class="form-check">
                                    <div class="checkbox">
                                        <label for="checkbox1" class="form-check-label ">
                                            {{implode(', ', $checklist->items->pluck('name')->toArray())}}
                                        </label>
                                    </div>
                               
                                </div>
                            
                        </div>
                    </div>                    
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
@endsection