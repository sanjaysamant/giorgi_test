@extends('admin.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE -->
            <h3 class="title-5 m-b-35">{{ucfirst($user->name)}} ({{implode(', ', $user->roles->pluck('name')->toArray())}})</h3>
            <div class="table-responsive table-responsive-data2">
                <div class="row">
                @if($user->tasks->count())
                    @foreach($user->tasks as $task)
                        <div class="col-md-6">
                            <h3 class="title-5 m-b-35">{{$task->checklist->name}} ({{$task->checklist->start_date}} To {{$task->checklist->end_date}})</h3>
                        	<ul class="list-group">
                        		@foreach($task->checklist->items as $item)
            						<li class="list-group-item justify-content-between">
            							{{$item->name}}
            							{!! !empty($task) ? (in_array($item->id, $task->items->pluck('id')->toArray()) ? "<span class='badge badge-success badge-pill'>Completed</span>" : "<span class='badge badge-warning badge-pill'>Incomplete</span>") : "<span class='badge badge-warning badge-pill'>Incomplete</span>"!!}
            						</li>
            					@endforeach
            				</ul>
                        </div>
                    @endforeach
                @else
                    No record
                @endif
                </div>
            </div>
            <!-- END DATA TABLE -->
        </div>
    </div>
@endsection
